%Matlab version: R2011a
%Author: Beniamin Str�czek
%Date: 03-12-2019
%
%Code for analizing weather data from online websites

%import jsonlab library
path = 'C:\Users\student\Desktop\BS\Matlab\jsonlab-1.9\jsonlab';
addpath(path);

%create csv file with proper headers
%check if such file already exists
if exist('weatherData.csv') == 2
    % do nothing and skip this part
else
    %create it and add headers
    file_name = 'weatherData.csv';
    header = {'stacja','data pomiaru','godzina pomiaru','temperatura','cisnienie','predkosc wiatru','wilgotnosc wzgledna','temperatura odczuwalna'};
    header_units = {'','','','�C','hPa','km/h','%','�C'};
    data_file_ID = fopen(file_name, 'w+');
    fprintf(data_file_ID, '%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s\n','"',header{1},'";"',header{2},'";"',header{3},'";"',header{4},'";"',header{5},'";"',header{6},'";"',header{7},'";"',header{8},'"');
    fprintf(data_file_ID, '%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s\n','"',header_units{1},'";"',header_units{2},'";"',header_units{3},'";"',header_units{4},'";"',header_units{5},'";"',header_units{6},'";"',header_units{7},'";"',header_units{8},'"');
    fclose(data_file_ID);
end


%start an infinity loop
while 1 == 1
    %load data from websites
    dane_raciborz_imgw = loadjson(urlread('https://danepubliczne.imgw.pl/api/data/synop/station/raciborz/format/json'));
    dane_czestochowa_imgw = loadjson(urlread('https://danepubliczne.imgw.pl/api/data/synop/station/czestochowa/format/json'));
    dane_opole_imgw = loadjson(urlread('https://danepubliczne.imgw.pl/api/data/synop/station/opole/format/json'));
    
    %prepare (create) cell structures to organise data
    raciborz_imgw = cell(7,2);
    czestochowa_imgw = cell(7,2);
    opole_imgw = cell(7,2);
    
    %and asign data to cells
    raciborz_imgw{1,2} = dane_raciborz_imgw.data_pomiaru;
    raciborz_imgw{2,2} = dane_raciborz_imgw.godzina_pomiaru;
    raciborz_imgw{3,2} = str2num(dane_raciborz_imgw.temperatura);
    raciborz_imgw{4,2} = str2num(dane_raciborz_imgw.cisnienie);
    raciborz_imgw{5,2} = str2num(dane_raciborz_imgw.predkosc_wiatru);
    raciborz_imgw{6,2} = str2num(dane_raciborz_imgw.wilgotnosc_wzgledna);

    czestochowa_imgw{1,2} = dane_czestochowa_imgw.data_pomiaru;
    czestochowa_imgw{2,2} = dane_czestochowa_imgw.godzina_pomiaru;
    czestochowa_imgw{3,2} = str2num(dane_czestochowa_imgw.temperatura);
    czestochowa_imgw{4,2} = str2num(dane_czestochowa_imgw.cisnienie);
    czestochowa_imgw{5,2} = str2num(dane_czestochowa_imgw.predkosc_wiatru);
    czestochowa_imgw{6,2} = str2num(dane_czestochowa_imgw.wilgotnosc_wzgledna);

    opole_imgw{1,2} = dane_opole_imgw.data_pomiaru;
    opole_imgw{2,2} = dane_opole_imgw.godzina_pomiaru;
    opole_imgw{3,2} = str2num(dane_opole_imgw.temperatura);
    opole_imgw{4,2} = str2num(dane_opole_imgw.cisnienie);
    opole_imgw{5,2} = str2num(dane_opole_imgw.predkosc_wiatru);
    opole_imgw{6,2} = str2num(dane_opole_imgw.wilgotnosc_wzgledna);

    raciborz_imgw{1,1} = 'data_pomiaru';
    raciborz_imgw{2,1} = 'godzina_pomiaru';
    raciborz_imgw{3,1} = 'temperatura';
    raciborz_imgw{4,1} = 'cisnienie';
    raciborz_imgw{5,1} = 'predkosc_wiatru';
    raciborz_imgw{6,1} = 'wilgotnosc_wzgledna';

    czestochowa_imgw{1,1} = 'data_pomiaru';
    czestochowa_imgw{2,1} = 'godzina_pomiaru';
    czestochowa_imgw{3,1} = 'temperatura';
    czestochowa_imgw{4,1} = 'cisnienie';
    czestochowa_imgw{5,1} = 'predkosc_wiatru';
    czestochowa_imgw{6,1} = 'wilgotnosc_wzgledna';

    opole_imgw{1,1} = 'data_pomiaru';
    opole_imgw{2,1} = 'godzina_pomiaru';
    opole_imgw{3,1} = 'temperatura';
    opole_imgw{4,1} = 'cisnienie';
    opole_imgw{5,1} = 'predkosc_wiatru';
    opole_imgw{6,1} = 'wilgotnosc_wzgledna';

    dane_raciborz_wttr = loadjson(urlread('http://wttr.in/raciborz?format=j1'));
    dane_czestochowa_wttr = loadjson(urlread('http://wttr.in/czestochowa?format=j1'));
    dane_opole_wttr = loadjson(urlread('http://wttr.in/opole?format=j1'));

    raciborz_wttr = cell(7,2);
    czestochowa_wttr = cell(7,2);
    opole_wttr = cell(7,2);

    raciborz_wttr{1,2} = dane_raciborz_wttr.weather{1,1}.date;
    raciborz_wttr{2,2} = dane_raciborz_wttr.current_condition{1,1}.observation_time;
    raciborz_wttr{3,2} = str2num(dane_raciborz_wttr.current_condition{1,1}.temp_C);
    raciborz_wttr{4,2} = str2num(dane_raciborz_wttr.current_condition{1,1}.pressure);
    raciborz_wttr{5,2} = str2num(dane_raciborz_wttr.current_condition{1,1}.windspeedKmph);
    raciborz_wttr{6,2} = str2num(dane_raciborz_wttr.current_condition{1,1}.humidity);
    raciborz_wttr{7,2} = str2num(dane_raciborz_wttr.current_condition{1,1}.FeelsLikeC);

    czestochowa_wttr{1,2} = dane_czestochowa_wttr.weather{1,1}.date;
    czestochowa_wttr{2,2} = dane_czestochowa_wttr.current_condition{1,1}.observation_time;
    czestochowa_wttr{3,2} = str2num(dane_czestochowa_wttr.current_condition{1,1}.temp_C);
    czestochowa_wttr{4,2} = str2num(dane_czestochowa_wttr.current_condition{1,1}.pressure);
    czestochowa_wttr{5,2} = str2num(dane_czestochowa_wttr.current_condition{1,1}.windspeedKmph);
    czestochowa_wttr{6,2} = str2num(dane_czestochowa_wttr.current_condition{1,1}.humidity);
    czestochowa_wttr{7,2} = str2num(dane_czestochowa_wttr.current_condition{1,1}.FeelsLikeC);

    opole_wttr{1,2} = dane_opole_wttr.weather{1,1}.date;
    opole_wttr{2,2} = dane_opole_wttr.current_condition{1,1}.observation_time;
    opole_wttr{3,2} = str2num(dane_opole_wttr.current_condition{1,1}.temp_C);
    opole_wttr{4,2} = str2num(dane_opole_wttr.current_condition{1,1}.pressure);
    opole_wttr{5,2} = str2num(dane_opole_wttr.current_condition{1,1}.windspeedKmph);
    opole_wttr{6,2} = str2num(dane_opole_wttr.current_condition{1,1}.humidity);
    opole_wttr{7,2} = str2num(dane_opole_wttr.current_condition{1,1}.FeelsLikeC);

    raciborz_wttr{1,1} = 'data_pomiaru';
    raciborz_wttr{2,1} = 'godzina_pomiaru';
    raciborz_wttr{3,1} = 'temperatura';
    raciborz_wttr{4,1} = 'cisnienie';
    raciborz_wttr{5,1} = 'predkosc_wiatru';
    raciborz_wttr{6,1} = 'wilgotnosc_wzgledna';
    raciborz_wttr{7,1} = 'temperatura_odczuwalna';

    czestochowa_wttr{1,1} = 'data_pomiaru';
    czestochowa_wttr{2,1} = 'godzina_pomiaru';
    czestochowa_wttr{3,1} = 'temperatura';
    czestochowa_wttr{4,1} = 'cisnienie';
    czestochowa_wttr{5,1} = 'predkosc_wiatru';
    czestochowa_wttr{6,1} = 'wilgotnosc_wzgledna';
    czestochowa_wttr{7,1} = 'temperatura_odczuwalna';

    opole_wttr{1,1} = 'data_pomiaru';
    opole_wttr{2,1} = 'godzina_pomiaru';
    opole_wttr{3,1} = 'temperatura';
    opole_wttr{4,1} = 'cisnienie';
    opole_wttr{5,1} = 'predkosc_wiatru';
    opole_wttr{6,1} = 'wilgotnosc_wzgledna';
    opole_wttr{7,1} = 'temperatura_odczuwalna';

    %calculate 'feels like' temperature and asign to cells
    raciborz_imgw{7,1} = 'temperatura_odczuwalna';
    czestochowa_imgw{7,1} = 'temperatura_odczuwalna';
    opole_imgw{7,1} = 'temperatura_odczuwalna';
    raciborz_imgw{7,2} = ( 33 + (0.478 + (0.237 * sqrt(str2double(dane_raciborz_imgw.predkosc_wiatru))) - (0.0124 * str2double(dane_raciborz_imgw.predkosc_wiatru))) * ((str2double(dane_raciborz_imgw.temperatura) - 33)));
    czestochowa_imgw{7,2} = ( 33 + (0.478 + (0.237 * sqrt(str2double(dane_czestochowa_imgw.predkosc_wiatru))) - (0.0124 * str2double(dane_czestochowa_imgw.predkosc_wiatru))) * ((str2double(dane_czestochowa_imgw.temperatura) - 33)));
    opole_imgw{7,2} = ( 33 + (0.478 + (0.237 * sqrt(str2double(dane_opole_imgw.predkosc_wiatru))) - (0.0124 * str2double(dane_opole_imgw.predkosc_wiatru))) * ((str2double(dane_opole_imgw.temperatura) - 33)));

    %write data to csv file
    file_name = 'weatherData.csv';
    data_file_ID = fopen(file_name, 'a');
    fprintf(data_file_ID, '%s%s%s%s%s%s%s%f%s%f%s%f%s%f%s%f%s\n','"','raciborz_imgw','";"',raciborz_imgw{1,2},'";"',raciborz_imgw{2,2},'";"',raciborz_imgw{3,2},'";"',raciborz_imgw{4,2},'";"',raciborz_imgw{5,2},'";"',raciborz_imgw{6,2},'";"',raciborz_imgw{7,2},'"');
    fprintf(data_file_ID, '%s%s%s%s%s%s%s%f%s%f%s%f%s%f%s%f%s\n','"','raciborz_wttr','";"',raciborz_wttr{1,2},'";"',raciborz_wttr{2,2},'";"',raciborz_wttr{3,2},'";"',raciborz_wttr{4,2},'";"',raciborz_wttr{5,2},'";"',raciborz_wttr{6,2},'";"',raciborz_wttr{7,2},'"');
    fprintf(data_file_ID, '%s%s%s%s%s%s%s%f%s%f%s%f%s%f%s%f%s\n','"','czestochowa_imgw','";"',czestochowa_imgw{1,2},'";"',czestochowa_imgw{2,2},'";"',czestochowa_imgw{3,2},'";"',czestochowa_imgw{4,2},'";"',czestochowa_imgw{5,2},'";"',czestochowa_imgw{6,2},'";"',czestochowa_imgw{7,2},'"');
    fprintf(data_file_ID, '%s%s%s%s%s%s%s%f%s%f%s%f%s%f%s%f%s\n','"','czestochowa_wttr','";"',czestochowa_wttr{1,2},'";"',czestochowa_wttr{2,2},'";"',czestochowa_wttr{3,2},'";"',czestochowa_wttr{4,2},'";"',czestochowa_wttr{5,2},'";"',czestochowa_wttr{6,2},'";"',czestochowa_wttr{7,2},'"');
    fprintf(data_file_ID, '%s%s%s%s%s%s%s%f%s%f%s%f%s%f%s%f%s\n','"','opole_imgw','";"',opole_imgw{1,2},'";"',opole_imgw{2,2},'";"',opole_imgw{3,2},'";"',opole_imgw{4,2},'";"',opole_imgw{5,2},'";"',opole_imgw{6,2},'";"',opole_imgw{7,2},'"');
    fprintf(data_file_ID, '%s%s%s%s%s%s%s%f%s%f%s%f%s%f%s%f%s\n','"','opole_wttr','";"',opole_wttr{1,2},'";"',opole_wttr{2,2},'";"',opole_wttr{3,2},'";"',opole_wttr{4,2},'";"',opole_wttr{5,2},'";"',opole_wttr{6,2},'";"',opole_wttr{7,2},'"');
    fclose(data_file_ID);

    %find maximum values in te csv file
    data_for_finding_maxes = fopen(file_name, 'r');
    %create a matrix containing data from file
    M1 = textscan(data_for_finding_maxes, '%q %q %q %q %q %q %q %q', 'Delimiter', ';');
    %check the size of that matrix
    [R,C] = size(M1{1});
    %create a matrix that will contain only numerical values from file
    M2 = zeros(R-2,5);
    
    %add values to matrix M2
    for j = 1 : 5
        for i = 1 : (R-2)
            M2(i, j) = str2double(M1{j+3}{i+2});
        end
    end

    fclose(data_for_finding_maxes);
    %find maximum values in proper columns of M2 matrix
    max_temperatura = max(M2(:,1));
    max_cisnienie = max(M2(:,2));
    max_predkosc_wiatru = max(M2(:,3));
    max_wilgotnosc_wzgledna = max(M2(:,4));
    max_temperatura_odczuwalna = max(M2(:,5));

    %get 'last' (meaning most recently occured) coordinates of those maximum values 
    [r_tem,c_tem] = find(M2(:,1) == max_temperatura, 1, 'last');
    [r_cis,c_cis] = find(M2(:,2) == max_cisnienie, 1, 'last');
    [r_pre,c_pre] = find(M2(:,3) == max_predkosc_wiatru, 1, 'last');
    [r_wil,c_wil] = find(M2(:,4) == max_wilgotnosc_wzgledna, 1, 'last');
    [r_odc,c_odc] = find(M2(:,5) == max_temperatura_odczuwalna, 1, 'last');

    %display gathered data
    fprintf('\nThe highest so far noted temperature equals: %.2f �C', max_temperatura);
    fprintf('\nand it occured on %s at %s\n\n', M1{c_tem+1}{r_tem+2}, M1{c_tem+2}{r_tem+2});

    fprintf('The highest so far noted pressure equals: %.2f hPa', max_cisnienie);
    fprintf('\nand it occured on %s at %s\n\n', M1{c_cis+1}{r_cis+2}, M1{c_cis+2}{r_cis+2});

    fprintf('The highest so far noted wind speed equals: %.2f km/h', max_predkosc_wiatru);
    fprintf('\nand it occured on %s at %s\n\n', M1{c_pre+1}{r_pre+2}, M1{c_pre+2}{r_pre+2});

    fprintf('The highest so far noted relative humidity equals: %.2f %%', max_wilgotnosc_wzgledna);
    fprintf('\nand it occured on %s at %s\n\n', M1{c_wil+1}{r_wil+2}, M1{c_wil+2}{r_wil+2});

    fprintf('The highest so far noted "feels like" temperature equals: %.2f %%', max_temperatura_odczuwalna);
    fprintf('\nand it occured on %s at %s\n\n', M1{c_odc+1}{r_odc+2}, M1{c_odc+2}{r_odc+2});
    
    %set the loop to run once an hour
    pause(3600);
end